package Vo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-04-30T18:58:25")
@StaticMetamodel(Archivosfacturassubidas.class)
public class Archivosfacturassubidas_ { 

    public static volatile SingularAttribute<Archivosfacturassubidas, Long> idarchivosubido;
    public static volatile SingularAttribute<Archivosfacturassubidas, String> documentoarchivosubido;
    public static volatile SingularAttribute<Archivosfacturassubidas, Date> fechacreacion;
    public static volatile SingularAttribute<Archivosfacturassubidas, byte[]> archivo;

}