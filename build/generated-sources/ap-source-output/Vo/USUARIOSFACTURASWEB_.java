package Vo;

import Vo.Cliente;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-04-30T18:58:25")
@StaticMetamodel(USUARIOSFACTURASWEB.class)
public class USUARIOSFACTURASWEB_ { 

    public static volatile SingularAttribute<USUARIOSFACTURASWEB, Boolean> clienteNuevo;
    public static volatile SingularAttribute<USUARIOSFACTURASWEB, String> password;
    public static volatile SingularAttribute<USUARIOSFACTURASWEB, Cliente> idCliente;
    public static volatile SingularAttribute<USUARIOSFACTURASWEB, Date> fechaRegistro;
    public static volatile SingularAttribute<USUARIOSFACTURASWEB, Boolean> sendMail;
    public static volatile SingularAttribute<USUARIOSFACTURASWEB, String> documento;
    public static volatile SingularAttribute<USUARIOSFACTURASWEB, Long> idUsuarioFacturasWeb;

}