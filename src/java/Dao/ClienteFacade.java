/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Vo.Cliente;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author matc_
 */
@Stateless
public class ClienteFacade extends AbstractFacade<Cliente> {

    @PersistenceContext(unitName = "UniPereiraFacturasPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ClienteFacade() {
        super(Cliente.class);
    }
    
      public boolean loginCedula(Cliente account) {
        Cliente c = null;
        try {
            Query query = em.createQuery("SELECT c FROM Cliente c WHERE c.documento = ?1");
            query.setParameter(1, account.getDocumento());
            List<Cliente> listCliente = query.getResultList();
            if (!listCliente.isEmpty()) {
                return true;
            }
        } catch (Exception localException) {
        }
        return false;
    }

    public Long retirarId(String cedula) {
        Cliente c = null;
        Long id = new Long("0");
        try {
            Query query = em.createQuery("SELECT c FROM Cliente c WHERE c.documento = ?1");
            query.setParameter(1, cedula);
            List<Cliente> listCliente = query.getResultList();
            if (!listCliente.isEmpty()) {
                id = ((Cliente) listCliente.get(0)).getIdCliente();
            }
        } catch (Exception localException) {
        }
        return id;
    }

    public Cliente findByCedula(String cedula) {
        return (Cliente) em.createQuery("SELECT c FROM Cliente c WHERE c.documento = ?1").setParameter(1, cedula)
                .getResultList().stream().findFirst().orElse(null);
    }
}
