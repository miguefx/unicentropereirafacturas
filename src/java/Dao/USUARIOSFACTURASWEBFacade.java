/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Vo.USUARIOSFACTURASWEB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author matc_
 */
@Stateless
public class USUARIOSFACTURASWEBFacade extends AbstractFacade<USUARIOSFACTURASWEB> {

    @PersistenceContext(unitName = "UniPereiraFacturasPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public USUARIOSFACTURASWEBFacade() {
        super(USUARIOSFACTURASWEB.class);
    }
    
    public USUARIOSFACTURASWEB login(String cedula, String password) {
        
        return (USUARIOSFACTURASWEB) em.createNamedQuery("USUARIOSFACTURASWEB.findByCedulaXPassword").setParameter("cedula", cedula)
                .setParameter("password", password)
                .getResultList().stream().findFirst().orElse(null);
        
    }

    public boolean existeUsuario(String documento) {
        return em.createNamedQuery("USUARIOSFACTURASWEB.findByCedula")
                .setParameter("documento", documento)
                .getResultList().isEmpty();
    }
    public USUARIOSFACTURASWEB findByDocumento(String documento) {
        return (USUARIOSFACTURASWEB) em.createNamedQuery("USUARIOSFACTURASWEB.findByCedula")
                .setParameter("documento", documento)
                .getResultList().stream().findFirst().orElse(null);
    }
    
}
