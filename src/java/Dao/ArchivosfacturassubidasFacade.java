/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Vo.Archivosfacturassubidas;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author matc_
 */
@Stateless
public class ArchivosfacturassubidasFacade extends AbstractFacade<Archivosfacturassubidas> {

    @PersistenceContext(unitName = "UniPereiraFacturasPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ArchivosfacturassubidasFacade() {
        super(Archivosfacturassubidas.class);
    }
      public Long traerUltimo() {
        try {
            TypedQuery<Long> query = em.createQuery("SELECT MAX(a.idarchivosubido) from Archivosfacturassubidas a ", Long.class);
            return query.getSingleResult();
        } catch (Exception e) {
        }
        return null;
    }
}
