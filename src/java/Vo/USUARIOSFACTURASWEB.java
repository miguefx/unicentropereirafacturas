/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 *
 * @author ASUS
 */

@EqualsAndHashCode(of = {"idUsuarioFacturasWeb"})
@ToString(of = {"idUsuarioFacturasWeb"}, includeFieldNames = true)
@Entity
@Table(name = "USUARIOSFACTURASWEB")
@Data
@NamedQueries({
    @NamedQuery(name = "USUARIOSFACTURASWEB.findByCedulaXPassword", query = "SELECT t FROM USUARIOSFACTURASWEB t where t.documento = :cedula  and t.password =  :password"),
    @NamedQuery(name = "USUARIOSFACTURASWEB.findByCedula", query = "SELECT t FROM USUARIOSFACTURASWEB t where t.documento = :documento")})
public class USUARIOSFACTURASWEB implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDUSUARIOFACTURAWEB")
    private Long idUsuarioFacturasWeb;
    
    
    @ManyToOne
    @JoinColumn( name = "IDCLIENTE" , referencedColumnName = "IDCLIENTE"  )
    private Cliente idCliente;
    
    
    @Column(name = "DOCUMENTO")
    private String documento;
    
    @Column(name = "PASSWORD")
    private String password;
    
    @Column(name = "FECHAREGISTRO")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fechaRegistro;
    
    @Column(name = "CLIENTENUEVO")
    private Boolean clienteNuevo;
    
    @Column(name = "SENDMAIL")
    private Boolean sendMail;

    

}
