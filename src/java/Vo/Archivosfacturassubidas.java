/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 *
 * @author matc_
 */
@EqualsAndHashCode(of = {"idarchivosubido"})
@ToString(of = {"idarchivosubido"}, includeFieldNames = true)
@Entity
@Data
@Table(name="ARCHIVOSFACTURASSUBIDAS")
public class Archivosfacturassubidas implements Serializable {

   private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    @Column(name = "IDARCHIVOSUBIDO",insertable = false)
    private Long idarchivosubido;
    @Size(max = 50)
    @Column(name = "DOCUMENTOARCHIVOSUBIDO")
    private String documentoarchivosubido;
    @Column(name = "FECHACREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechacreacion;
    @Lob
    @Column(name = "ARCHIVO")
    private byte[] archivo;

}
