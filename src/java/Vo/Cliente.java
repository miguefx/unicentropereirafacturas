/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author matc_
 */
@EqualsAndHashCode(of = {"idCliente"})
@ToString(of = {"idCliente"}, includeFieldNames = true)
@Entity
@Table(name = "CLIENTE")
@NamedQueries({
    @NamedQuery(name = "Cliente.findByDocumento", query = "SELECT c FROM Cliente c WHERE c.documento = :documento")})

public class Cliente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDCLIENTE")
    @Getter
    @Setter
    private Long idCliente;

    @Getter
    @Setter
    @Column(name = "NOMBRE1")
    private String nombre1;

    @Getter
    @Setter
    @Column(name = "NOMBRE2")
    private String nombre2;

    @Getter
    @Setter
    @Column(name = "APELLIDO1")
    private String apellido1;

    @Getter
    @Setter
    @Column(name = "APELLIDO2")
    private String apellido2;

    @Getter
    @Setter
    @Column(name = "DOCUMENTO")
    private String documento;

    @Getter
    @Setter
    @Column(name = "TELEFONO1")
    private String telefono;

    @Getter
    @Setter
    @Column(name = "TELEFONO2")
    private String celular;
    
    @Getter
    @Setter
    @Column(name = "EMAIL")
    private String email;
    
    @Getter
    @Setter
    @Column(name = "IDSEXO")
    private Integer genero;

    public Cliente() {
    }

}
