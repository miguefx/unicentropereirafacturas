/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import Dao.ClienteFacade;
import Vo.Cliente;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Miguel-PC
 */
@ManagedBean(name = "registroBean")
@ViewScoped
public class registroBean {

    @EJB
    private ClienteFacade objDAOCliente;

    @Getter
    @Setter
    private Cliente clienteNuevo;
    @Getter
    @Setter
    private Boolean terminos;

    public registroBean() {
    }

    @PostConstruct
    public void init() {
        clienteNuevo = new Cliente();
    }

    public String lanzarRegistroPag() {
        return terminos ? "frmRegistro" : "frmLoginUsuarioCedula";
    }

    public void registrarCliente() {
        try {
            objDAOCliente.create(clienteNuevo);
            notificarInformacion("Se ha creado exitosamente el cliente");
        } catch (Exception e) {
            System.out.println("Ocurrio error" + e);
            notificarInformacion("Ocurrio un error al crear el cliente");
        }
    }

    private void notificarInformacion(String mensage) {
        FacesMessage msg = new FacesMessage(mensage);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}
