/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import Dao.ClienteFacade;
import Dao.USUARIOSFACTURASWEBFacade;
import Dao.ArchivosfacturassubidasFacade;
import Vo.Cliente;
import Vo.Usuarios;
import Vo.Archivosfacturassubidas;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.util.Messages;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author matc_
 */
@Named(value = "beanLogin")
@SessionScoped
public class beanLogin implements Serializable {

    private String destination = "D:\\Data\\FotosFacturasCC\\EstacionIbague\\";
    private String sesion;

    @Getter
    @Setter
    private List<String> listArchivosCargados;

    @Getter
    @Setter
    private Boolean renderNew;

    @Getter
    @Setter
    private String respuestaSubida;

    @Getter
    @Setter
    private String password;

    @Getter
    @Setter
    private String passwordNuevo;

    @Getter
    @Setter
    private String passwordAntiguo;

    @Getter
    @Setter
    private Cliente objVoClie;

    @EJB
    USUARIOSFACTURASWEBFacade objDaoUsuariosFacturasWeb;

    @Getter
    @Setter
    private Boolean renderOld;

    @Getter
    @Setter
    private String cedula;

    @Getter
    @Setter
    private String usuario;
    @Getter
    @Setter
    private String contrase\u00f1a;
    @Getter
    @Setter
    private String nombre1;
    @Getter
    @Setter
    private String imagenFactura;
    @Getter
    @Setter
    private String nombre2;
    @Getter
    @Setter
    private String apellido1;
    @Getter
    @Setter
    private String apellido2;
    @Getter
    @Setter
    private String email;
    @Getter
    @Setter
    private String telefono;
    @Getter
    @Setter
    private String celular;
    @Getter
    @Setter
    private Integer tipoDocumento;
    @Getter
    @Setter
    private Long idEvento = new Long("0");

    private static String nombreUser;

    @Getter
    @Setter
    UploadedFile file;

    @Getter
    @Setter
    private Cliente objVOCliente;
    @Getter
    @Setter
    private Cliente seleccionCliente;
    @Getter
    @Setter
    private Usuarios objVOUsuarios;

    @Getter
    @Setter
    private Archivosfacturassubidas objVOArchivosFacturasSubidas;
    @EJB
    private ClienteFacade objDAOCliente;

    @EJB
    private ArchivosfacturassubidasFacade objDaoArchivosFacturas;

    @PostConstruct
    public void init() {
        listArchivosCargados = new ArrayList<>();
    }


    public void handleFileUpload(FileUploadEvent event) {
        try {

            InputStream stream = event.getFile().getInputstream();
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            objVOArchivosFacturasSubidas = new Archivosfacturassubidas();
            for (int length = 0; (length = stream.read(buffer)) > 0;) {
                output.write(buffer, 0, length);
                objVOArchivosFacturasSubidas.setArchivo(output.toByteArray());
            }
            objVOArchivosFacturasSubidas.setFechacreacion(new Date());
            objVOArchivosFacturasSubidas.setDocumentoarchivosubido(nombreUser);
            System.out.println("Va a crear");
            objDaoArchivosFacturas.create(objVOArchivosFacturasSubidas);
            System.out.println("Creo");
            listArchivosCargados.add(event.getFile().getFileName());
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Se ha subido el archivo " + event.getFile().getFileName() + " Exitosamente"));
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void actualizar() {
        System.out.println("Holi");
        this.listArchivosCargados = listArchivosCargados;
    }

    public void upload(ActionEvent event) throws IOException {
        InputStream stream = file.getInputstream();
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        try {
            for (int length = 0; (length = stream.read(buffer)) > 0;) {
                output.write(buffer, 0, length);
                objVOArchivosFacturasSubidas = new Archivosfacturassubidas();
                objVOArchivosFacturasSubidas.setFechacreacion(new Date());
                objVOArchivosFacturasSubidas.setDocumentoarchivosubido(nombreUser);
                objVOArchivosFacturasSubidas.setArchivo(output.toByteArray());
            }
        } catch (IOException ex) {
        }
    }

    public static String getNombreUser() {
        return nombreUser;
    }

    public static void setNombreUser(String nombreUser) {
        nombreUser = nombreUser;
    }

    public void renderContenido(String desde) {

        switch (desde) {
            case "New":
                renderNew = Boolean.TRUE;
                renderOld = Boolean.FALSE;
                break;
            case "Old":
                renderNew = Boolean.FALSE;
                renderOld = Boolean.TRUE;
                break;
        }
    }

    public void loginCedula(ActionEvent egt) {
        try {

            objVoClie = objDAOCliente.findByCedula(cedula);

            if (Objects.nonNull(objVoClie)) {
                nombreUser = objVoClie.getDocumento();
                seleccionCliente = objVoClie;
                HttpSession hs = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
                hs.setAttribute(
                        "usr", objVoClie.getDocumento());

                FacesContext contex = FacesContext.getCurrentInstance();
                contex.getExternalContext().redirect("/UniPereiraFacturas/faces/frmDatosCuenta.xhtml");

            } else {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("El documento ingresado no se encuentra registrado"));
            }

        } catch (Exception contex) {
            // empty catch block
        }
    }

    public void session() {
        try {
            this.objVOUsuarios = (Usuarios) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usr");
            if (this.objVOUsuarios == null) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("/UniPereiraFacturas/faces/frmLoginUsuarioCedula.xhtml");
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("Debe iniciar Session Primero"));
            }
        } catch (Exception context) {
            // empty catch block
        }
    }

//    public void registrarCliente(ActionEvent egt) {
//        try {
//            this.objVOCliente = new Cliente();
//            this.objVOCliente.setApellido1(this.apellido1);
//            this.objVOCliente.setApellido2(this.apellido2);
//            this.objVOCliente.setEmail(this.email);
//            this.objVOCliente.setTelefono1(this.telefono);
//            this.objVOCliente.setTelefono2(this.celular);
//            this.objVOCliente.setNombre1(this.nombre1);
//            this.objVOCliente.setNombre2(this.nombre2);
//            this.objVOCliente.setDocumento(this.cedula);
//            this.objVOCliente.setIdtipodocumento(this.tipoDocumento);
//            this.objVOCliente.setFechacreacion(new Date());
//            this.objDAOCliente.create(this.objVOCliente);
//            if (this.objVOCliente != null) {
//                Long auxCedula = this.objDAOCliente.retirarId(this.cedula);
//                this.seleccionCliente = (Cliente) this.objDAOCliente.find((Object) auxCedula);
//                this.nombre1 = this.seleccionCliente.getNombre1();
//                this.nombre2 = this.seleccionCliente.getNombre2();
//                this.apellido1 = this.seleccionCliente.getApellido1();
//                this.apellido2 = this.seleccionCliente.getApellido2();
//                this.email = this.seleccionCliente.getEmail();
//                this.telefono = this.seleccionCliente.getTelefono1();
//                this.celular = this.seleccionCliente.getTelefono2();
//                this.tipoDocumento = this.seleccionCliente.getIdtipodocumento();
//                FacesContext contex = FacesContext.getCurrentInstance();
//                contex.getExternalContext().redirect("/UniPereiraFacturas/faces/frmDatosCuenta.xhtml");
//            } else {
//                FacesContext context = FacesContext.getCurrentInstance();
//                context.addMessage(null, new FacesMessage("No se logro registrar el cliente"));
//            }
//        } catch (Exception context) {
//            // empty catch block
//        }
//    }
    public void navBack() {
        try {
            this.cedula = "";
            this.nombre1 = "";
            this.nombre2 = "";
            this.apellido1 = "";
            this.apellido2 = "";
            this.celular = "";
            this.telefono = "";
            this.email = "";
            listArchivosCargados = new ArrayList<>();
            FacesContext contex = FacesContext.getCurrentInstance();
            contex.getExternalContext().redirect("/UniPereiraFacturas/faces/frmLoginUsuarioCedula.xhtml");
        } catch (Exception contex) {
            // empty catch block
        }
    }

    public beanLogin() {
    }

    private String lanzarEjecutableDeCorreo(String respuesta) {
        Runtime r = Runtime.getRuntime();
        Process p = null;
        try {
            p = r.exec("C:\\UniPereira\\EnvioClaveTemporal\\claveTemporal.exe");
        } catch (IOException ex) {
            System.out.println("Ocurrio un error lanzando el ejecutable");
        }
        return respuesta;
    }

    private void notificarMensajeCorreo() {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion !",
                "Verifica tu correo electronico encontraras la clave temporal para poder ingresar");
        Messages.addFlashGlobal(msg);
    }

    private void desplegarDialogoCambioContraseña() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('dlg2').show();");
    }

    private void notificarUsuarioConClaveTemporal() {
        FacesMessage msg = new FacesMessage("El documento ingresado ya existe, porfavor ingrese con sus credenciales.");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    private void notificarIngreseLaClaveTemporal() {
        FacesMessage msg = new FacesMessage("Ingresa la clave temporal enviada a tu correo.");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

}
